# Connecting to a AnyConnect VPN with Microsoft web based two-factor authorization using OpenConnect

Trying to connect a Linux client with openconnect to a AnyConnect VPN which uses Microsofts Web-based 2FA (using a SMS token), does not work out of the box.

The easy way would be to install the Cisco AnyConnect client but this taints your open source Linux installation.

Following python script allows it using the openconnect vpn client by first establishing a selenium controlled browser session which - after successful 2FA authentication - captures the webvpn authentication cookie and then starts openconnect using this cookie.

## Base Installation
 - *git clone* this repository

 - After changing into the project folder, symlink the *webvpn-openconnect.py* script to a folder which is in the search PATH, eg. into your privat *~/bin/* folder by:
```
     $ ln -s $PWD/webvpn-openconnect.py ~/bin/
```
 - Make the script executable if not already by:
```
     $ chmod +x webvpn-openconnect.py
```
 - Add following packages eg. when using Ubuntu/Debian by:
```
     $ sudo apt install python3-selenium  firefox-geckodriver
```
 - Try to connect the vpn server: *(change arguments according your vpn server and login)*
```
     $ webvpn-openconnect.py  vpn.mycompany.com  mylogin@mycompany.com
```
 - If this works, you should be able to connect to smb shares and/or hosts you habe access (...)

 - Terminate  the vpn session by:
```
     webvpn-openconnect.py -k
```

## Allow executing of *openconnect* without *sudo* password
 - **Attention**: if you make typos in this step, you exclude yourself to '*sudo*' access so you could only repaired it by booting the recovery console!
 - So save following file but don't close the editor unless you checked in console the '*sudo*' functionallity: if it does not work correct the entry in the open editor session or delete it at all! **You have been warned!!!**
 - Create a file '*/etc/sudoers.d/openconnect*' with follwing content:
```
%sudo ALL=NOPASSWD:/usr/sbin/openconnect
```
 - Depending on the system you might adapt %sudo or /usr/sbin/openconnect according your system configuration.
 
 - Open a new shell and run '*sudo openconnect*' - if you get the openconnect help without entering your password it works!


## Start/Stop the vpn via GNOME toolbar  
 - Install the GNOME extension "*Guillotine*" from:  
[https://extensions.gnome.org/extension/3981/guillotine/](https://extensions.gnome.org/extension/3981/guillotine/)

 - After clicking on the *Guillotine icon > Guillotine > Configuration ...*

 - And add folling lines in the Guillotine configuration directly under the line:  *"menu": [*
```	
   {
        "type": "switch",
        "title": "Connect/Disconnect VPN",
        "start": "webvpn-openconnect.py vpn.mycompany.com  mylogin@mycompany.com",
        "stop": "webvpn-openconnect.py -k -u",
        "check": "ps -C openconnect",
        "icon": "emblem-synchronizing-symbolic",
        "interval_s": 600
   },
   {
        "type": "command",
        "title": "Set/Change VPN Password",
        "command": "gnome-terminal -e 'webvpn-openconnect.py -p vpn.mycompany.com  mylogin@mycompany.com'",
        "instancing": "singleInstance",
        "icon": "emblem-documents-symbolic",
        "killOnDisable": false
   },
```

 - Replace both occurences of '*vpn.mycompany.com*' and '*mylogin@mycompany.com*' to your companys vpn and credentials.

 - After saving the configuration Start/Stop of the VPN should be possible via the Guillotine menu.

## Storing the vpn login password in the GNOME Keyring
   - Optionally and for convenience you may store the vpn password in your local keyring by clicking on the Guillotine menu item *Set/Change VPN Password* but mention your companys security policy! 

Have fun  
  Matthias
