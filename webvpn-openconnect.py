#! /usr/bin/python3

# Prerequisites: apt install  python3-selenium  firefox-geckodriver
# Remark: if this script will be executed with a normal sudo user-id, put 
#         '%sudo ALL=NOPASSWD:/usr/sbin/openconnect' 
#         in file /etc/sudoers.d/openconnect and chmod 400 this file!

import os, sys, keyring, time
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.common.keys import Keys
import urllib.parse

USER_AGENT = "Cisco AnyConnect VPN Agent for Windows 4.10.04071"

class WebVpnOpenconnect:
    def __init__(self):
        self.basename = os.path.basename(__file__)

    def KillOpenconnect(self, umountsmb=True):
        # kill openconnect by killing the (sudo) parent process, because this does not need sudo rights ;-)
        # todo: kill any open gvfs shares
        if umountsmb: os.system("gio mount -s smb")
        pid = os.popen('ps -C openconnect -o ppid=').read()
        if pid != '':
            os.system(f"kill {pid}")
            print('openconnect terminated');

    def SetPasswd(self, vpnserver, username):
        import getpass
        passwd = getpass.getpass(
            prompt="Be aware that entering the password in local keyring database is not "
            "needed and doing so puts your security at some risk.\nAlternatively you may close "
            "this window without entering/storing the password.\n\n"
            f"Password for account '{username}' at '{vpnserver}':")
        keyring.set_password(self.basename, vpnserver+":"+username, passwd)

    def Connect(self, vpnserver, username):
        self.KillOpenconnect()
        passwd = keyring.get_password(self.basename, vpnserver+":"+username)
        profile = webdriver.FirefoxProfile()
        profile.set_preference("general.useragent.override", USER_AGENT)
        profile.set_preference("javascript.enabled", False)
        driver = webdriver.Firefox(profile, executable_path='/usr/bin/geckodriver', 
                 firefox_binary=FirefoxBinary('/usr/lib/firefox/firefox'))
        driver.get('https://' + vpnserver)
        WebDriverWait(driver, 60).until(EC.visibility_of_element_located((By.NAME, "loginfmt")))
        uidbox = driver.find_element_by_name("loginfmt")
        uidbox.send_keys(username  + Keys.RETURN)
        if passwd:
            WebDriverWait(driver, 60).until(EC.visibility_of_element_located((By.NAME, "passwd")))
            passbox = driver.find_element_by_name("passwd")
            passbox.send_keys(passwd + Keys.RETURN)
        # after successful login, 2FA procecure needs user invention until 'webvpn' cookie appears ...
        webvpn=None
        while webvpn==None:
            time.sleep(1)
            # pass over "Stay signed in" on occurance of input type="submit" data-report-event="Signin_Submit"

            if driver.find_elements_by_xpath("//input[@name='DontShowAgain']") \
              and driver.find_elements_by_xpath("//input[@data-report-event='Signin_Submit']"):
                driver.find_element_by_xpath("//input[@data-report-event='Signin_Submit']").click()
            webvpn = driver.get_cookie('webvpn')
        #print(driver.get_cookies())
        #print(f"webvpn='{webvpn}'")
        url = driver.current_url
        urlfrag = urllib.parse.urlparse(url)
        driver.quit()
        cmd = f"echo '{webvpn['value']}' | /usr/bin/sudo -b /usr/sbin/openconnect --cookie-on-stdin -u {username}  {webvpn['domain']} 2>&1 >/tmp/openconnect.log"
        print("\nStarting openconnect...")
        #print(cmd)
        os.system(cmd)
        time.sleep(1)


wvo = WebVpnOpenconnect()
if len(sys.argv)>1 and sys.argv[1] == '-k':
    wvo.KillOpenconnect(True if len(sys.argv)>2 and sys.argv[2] == '-u' else False)
elif len(sys.argv)==4 and sys.argv[1] == '-p':
    wvo.SetPasswd(sys.argv[2], sys.argv[3])
elif len(sys.argv)==3:
    wvo.Connect(sys.argv[1], sys.argv[2])
else: 
    print(f"usage: {wvo.basename}  -k [-u] | [-p] vpn.mycompany.com my.login@mycompany.com\n"
           "  -k: kill running openconnect\n  -u: umount smb shares before kill\n  -p: save password in keyring\n  "
           "  else: connect to vpn.mycompany.com with login my.login@mycompany.com"
           "\nIf executed with restricted user-id, place '%sudo ALL=NOPASSWD:/usr/sbin/openconnect' in file /etc/sudoers.d/openconnect and chmod 400 this file!")
    
    

